package com.tsconsulting.dsubbotin.tm;

import com.tsconsulting.dsubbotin.tm.endpoint.*;
import com.tsconsulting.dsubbotin.tm.marker.SoapCategory;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class UserEndpointTest {

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    @NotNull
    private final UserEndpoint userEndpoint;

    @NotNull
    final private String userLogin = "test";

    @NotNull
    final private String userEmail = "g@m.ru";

    @NotNull
    private SessionDTO session;

    public UserEndpointTest() {
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
        @NotNull final UserEndpointService userEndpointService = new UserEndpointService();
        userEndpoint = userEndpointService.getUserEndpointPort();
    }

    @Before
    @SneakyThrows
    public void initializeTest() {
        session = sessionEndpoint.register(userLogin, "test", userEmail);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void setPasswordUser() {
        @NotNull final String oldPassword = userEndpoint.getUser(session).getPasswordHash();
        userEndpoint.setPasswordUser(session, "newPassword");
        @NotNull final String newPassword = userEndpoint.getUser(session).getPasswordHash();
        Assert.assertNotEquals(oldPassword, newPassword);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void updateByIdUser() {
        @NotNull final UserDTO oldUser = userEndpoint.getUser(session);
        Assert.assertNull(oldUser.getLastName());
        Assert.assertNull(oldUser.getFirstName());
        Assert.assertNull(oldUser.getMiddleName());
        Assert.assertEquals(userEmail, oldUser.getEmail());
        @NotNull final String lastName = "lastName",
                firstName = "firstName",
                middleName = "middleName",
                newEmail = "newEmail";
        userEndpoint.updateByIdUser(session, lastName, firstName, middleName, newEmail);
        @NotNull final UserDTO updatedUser = userEndpoint.getUser(session);
        Assert.assertEquals(lastName, updatedUser.getLastName());
        Assert.assertEquals(firstName, updatedUser.getFirstName());
        Assert.assertEquals(middleName, updatedUser.getMiddleName());
        Assert.assertEquals(newEmail, updatedUser.getEmail());
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void isLoginUser() {
        Assert.assertTrue(userEndpoint.isLoginUser(userLogin));
        Assert.assertFalse(userEndpoint.isLoginUser("otherLogin"));
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void getUser() {
        @NotNull final UserDTO user = userEndpoint.getUser(session);
        Assert.assertNotNull(user);
        Assert.assertEquals(userEmail, user.getEmail());
        Assert.assertEquals(userLogin, user.getLogin());
    }

    @After
    @SneakyThrows
    public void finalizeTest() {
        @NotNull final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();
        @NotNull final AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();
        Assert.assertTrue(sessionEndpoint.closeSession(session));
        @NotNull final SessionDTO tempSession = sessionEndpoint.openSession("admin", "admin");
        adminUserEndpoint.removeByLoginUser(tempSession, userLogin);
        Assert.assertTrue(sessionEndpoint.closeSession(tempSession));
    }

}
