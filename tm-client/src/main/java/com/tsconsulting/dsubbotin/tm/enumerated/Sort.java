package com.tsconsulting.dsubbotin.tm.enumerated;

import com.tsconsulting.dsubbotin.tm.comparator.ComparatorByCreated;
import com.tsconsulting.dsubbotin.tm.comparator.ComparatorByName;
import com.tsconsulting.dsubbotin.tm.comparator.ComparatorByStartDate;
import com.tsconsulting.dsubbotin.tm.comparator.ComparatorByStatus;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;

public enum Sort {

    CREATED("Sort by created", ComparatorByCreated.INSTANCE),
    NAME("Sort by name", ComparatorByName.INSTANCE),
    START_DATE("Sort by date start", ComparatorByStartDate.INSTANCE),
    STATUS("Sort by status", ComparatorByStatus.INSTANCE);

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator comparator;

    Sort(@NotNull String displayName, @NotNull Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @NotNull
    public Comparator getComparator() {
        return comparator;
    }

}
