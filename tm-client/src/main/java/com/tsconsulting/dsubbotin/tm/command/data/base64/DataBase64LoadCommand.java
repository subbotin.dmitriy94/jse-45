package com.tsconsulting.dsubbotin.tm.command.data.base64;

import com.tsconsulting.dsubbotin.tm.command.AbstractCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataBase64LoadCommand extends AbstractCommand {

    @Override
    public @NotNull String name() {
        return "data-load-base64";
    }

    @Override
    public @NotNull String description() {
        return "Load base64 data.";
    }

    @Override
    public void execute() {
        @Nullable SessionDTO session = endpointLocator.getSessionService().getSession();
        endpointLocator.getAdminEndpoint().loadBase64(session);
        TerminalUtil.printMessage("Load from base64 completed.");
    }

}
