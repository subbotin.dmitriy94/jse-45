package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.entity.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.entity.IUserRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import com.tsconsulting.dsubbotin.tm.entity.Project;
import com.tsconsulting.dsubbotin.tm.entity.User;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.repository.entity.ProjectRepository;
import com.tsconsulting.dsubbotin.tm.repository.entity.UserRepository;
import com.tsconsulting.dsubbotin.tm.service.ConnectionService;
import com.tsconsulting.dsubbotin.tm.service.PropertyService;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectRepositoryTest {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final User user;

    @NotNull
    private final Project project;

    @NotNull
    private final String projectId;

    @NotNull
    private final String projectName = "testProject";

    @NotNull
    private final String userId;

    @NotNull
    private final EntityManager entityManager;

    public ProjectRepositoryTest() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        entityManager = connectionService.getEntityManager();
        projectRepository = new ProjectRepository(entityManager);
        userRepository = new UserRepository(entityManager);
        user = new User();
        userId = user.getId();
        user.setLogin("guest");
        user.setPasswordHash(HashUtil.salt(3, "qwe", "guest"));
        project = new Project();
        projectId = project.getId();
        project.setUser(user);
        project.setName(projectName);
        @NotNull final String projectDescription = "testProject";
        project.setDescription(projectDescription);
        entityManager.getTransaction().begin();
        userRepository.create(user);
        entityManager.getTransaction().commit();
    }

    @Before
    public void initializeTest() {
        entityManager.getTransaction().begin();
        projectRepository.create(project);
        entityManager.getTransaction().commit();
    }

    @Test
    public void findProject() throws AbstractException {
        checkProject(projectRepository.findByName(userId, projectName));
        checkProject(projectRepository.findById(projectId));
        checkProject(projectRepository.findById(userId, projectId));
        checkProject(projectRepository.findByIndex(userId, 0));
        @NotNull final List<Project> projects = projectRepository.findAll(userId);
        Assert.assertEquals(projects.size(), 1);
    }

    private void checkProject(@Nullable final Project foundProject) {
        Assert.assertNotNull(foundProject);
        Assert.assertEquals(project.getId(), foundProject.getId());
        Assert.assertEquals(project.getName(), foundProject.getName());
        Assert.assertEquals(project.getDescription(), foundProject.getDescription());
        Assert.assertEquals(project.getUser().getId(), foundProject.getUser().getId());
        Assert.assertEquals(project.getStartDate(), foundProject.getStartDate());
    }

    @Test
    public void remove() {
        Assert.assertNotNull(project);
        entityManager.getTransaction().begin();
        projectRepository.remove(project);
        entityManager.getTransaction().commit();
        Assert.assertFalse(projectRepository.existById(userId, projectId));
    }

    @After
    public void finalizeTest() {
        entityManager.getTransaction().begin();
        projectRepository.clear(userId);
        userRepository.remove(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}