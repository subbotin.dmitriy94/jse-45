package com.tsconsulting.dsubbotin.tm.api.service.entity;

import com.tsconsulting.dsubbotin.tm.entity.AbstractEntity;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IService<E extends AbstractEntity> {

    @NotNull
    List<E> findAll() throws AbstractException;

    @NotNull
    E findById(@NotNull String id) throws AbstractException;

    @NotNull
    E findByIndex(int index) throws AbstractException;

    void clear() throws AbstractException;

    void addAll(@NotNull List<E> entities) throws AbstractException;

}