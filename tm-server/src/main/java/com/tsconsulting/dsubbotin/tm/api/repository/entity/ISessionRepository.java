package com.tsconsulting.dsubbotin.tm.api.repository.entity;

import com.tsconsulting.dsubbotin.tm.entity.Session;
import org.jetbrains.annotations.NotNull;

public interface ISessionRepository extends IRepository<Session> {

    void open(@NotNull Session session);

    void close(@NotNull Session session);

    boolean contains(@NotNull Session session);

    boolean existByUserId(@NotNull String userId);

    boolean existById(@NotNull String id);

    void clear(@NotNull String userId);

}
