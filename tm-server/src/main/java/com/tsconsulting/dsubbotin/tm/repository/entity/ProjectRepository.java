package com.tsconsulting.dsubbotin.tm.repository.entity;

import com.tsconsulting.dsubbotin.tm.api.repository.entity.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.entity.Project;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import java.util.List;

public class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        return entityManager
                .createQuery("FROM Project", Project.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        return entityManager
                .createQuery("FROM Project WHERE user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public Project findById(@NotNull final String id) throws AbstractException {
        return entityManager
                .createQuery("FROM Project WHERE id = :id", Project.class)
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public Project findById(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        return entityManager
                .createQuery("FROM Project WHERE user.id = :userId AND id = :id", Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public Project findByIndex(final int index) throws AbstractException {
        return entityManager
                .createQuery("FROM Project", Project.class)
                .setMaxResults(1)
                .setFirstResult(index)
                .getResultStream()
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public Project findByIndex(@NotNull final String userId, final int index) throws AbstractException {
        return entityManager
                .createQuery("FROM Project WHERE user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .setFirstResult(index)
                .getResultStream()
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public Project findByName(@NotNull final String userId, @NotNull final String name) throws AbstractException {
        return entityManager
                .createQuery("FROM Project WHERE user.id = :userId AND name = :name", Project.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getResultStream()
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE Project").executeUpdate();
    }

    @Override
    public void clear(@NotNull final String userId) {
        entityManager
                .createQuery("DELETE Project WHERE user.id = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String projectId) {
        return entityManager
                .createQuery("FROM Project WHERE user.id = :userId AND id = :id", Project.class)
                .setParameter("userId", userId)
                .setParameter("id", projectId)
                .getResultStream()
                .findFirst()
                .isPresent();
    }

}
