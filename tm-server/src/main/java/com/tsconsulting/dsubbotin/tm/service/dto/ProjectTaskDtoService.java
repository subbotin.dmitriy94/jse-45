package com.tsconsulting.dsubbotin.tm.service.dto;

import com.tsconsulting.dsubbotin.tm.api.repository.dto.IProjectDtoRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.dto.ITaskDtoRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.ILogService;
import com.tsconsulting.dsubbotin.tm.api.service.dto.IProjectTaskDtoService;
import com.tsconsulting.dsubbotin.tm.dto.ProjectDTO;
import com.tsconsulting.dsubbotin.tm.dto.TaskDTO;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.entity.TaskNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.repository.dto.ProjectDtoRepository;
import com.tsconsulting.dsubbotin.tm.repository.dto.TaskDtoRepository;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import java.util.List;

@RequiredArgsConstructor
public final class ProjectTaskDtoService implements IProjectTaskDtoService {

    @NotNull
    private final IConnectionService connectionService;

    @NotNull
    private final ILogService logService;

    @Override
    public void bindTaskToProject(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            isEmpty(projectRepository, taskRepository, userId, projectId, taskId);
            @NotNull final TaskDTO task = taskRepository.findById(userId, taskId);
            task.setProjectId(projectId);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unbindTaskFromProject(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            isEmpty(projectRepository, taskRepository, userId, projectId, taskId);
            @NotNull final TaskDTO task = taskRepository.findById(userId, taskId);
            task.setProjectId(null);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public List<TaskDTO> findAllTasksByProjectId(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            return taskRepository.findAllByProjectId(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeProjectById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            @NotNull final List<TaskDTO> tasks = taskRepository.findAllByProjectId(userId, id);
            entityManager.getTransaction().begin();
            for (TaskDTO task : tasks) task.setProjectId(null);
            @NotNull final ProjectDTO project = projectRepository.findById(userId, id);
            projectRepository.remove(project);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeProjectByIndex(
            @NotNull final String userId,
            final int index
    ) throws AbstractException {
        final int realIndex = index - 1;
        if (realIndex < 0) throw new IndexIncorrectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final ProjectDTO project = projectRepository.findByIndex(userId, realIndex);
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            @NotNull final List<TaskDTO> tasks = taskRepository.findAllByProjectId(userId, project.getId());
            entityManager.getTransaction().begin();
            for (TaskDTO task : tasks) task.setProjectId(null);
            projectRepository.remove(project);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeProjectByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException {
        if (EmptyUtil.isEmpty(name)) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final ProjectDTO project = projectRepository.findByName(userId, name);
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            @NotNull final List<TaskDTO> tasks = taskRepository.findAllByProjectId(userId, project.getId());
            entityManager.getTransaction().begin();
            for (TaskDTO task : tasks) task.setProjectId(null);
            projectRepository.remove(project);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAllProject(@NotNull String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            @NotNull List<ProjectDTO> projects = projectRepository.findAll(userId);
            entityManager.getTransaction().begin();
            for (ProjectDTO project : projects) taskRepository.removeAllTaskByProjectId(userId, project.getId());
            projectRepository.clear(userId);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

    private void isEmpty(
            @NotNull final IProjectDtoRepository projectRepository,
            @NotNull final ITaskDtoRepository taskRepository,
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) throws AbstractException {
        if (EmptyUtil.isEmpty(userId)) throw new EmptyIdException();
        if (EmptyUtil.isEmpty(projectId)) throw new EmptyIdException();
        if (EmptyUtil.isEmpty(taskId)) throw new EmptyIdException();
        if (!projectRepository.existById(userId, projectId)) throw new ProjectNotFoundException();
        if (!taskRepository.existById(userId, taskId)) throw new TaskNotFoundException();
    }

}
