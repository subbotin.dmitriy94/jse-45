package com.tsconsulting.dsubbotin.tm.api.repository.dto;

import com.tsconsulting.dsubbotin.tm.dto.AbstractEntityDTO;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IDtoRepository<E extends AbstractEntityDTO> {

    @NotNull
    List<E> findAll();

    @NotNull
    E findById(@NotNull String id) throws AbstractException;

    @NotNull
    E findByIndex(int index) throws AbstractException;

    void create(@NotNull E entity);

    void remove(@NotNull E entity);

    void update(@NotNull E entity);

    void clear();

}