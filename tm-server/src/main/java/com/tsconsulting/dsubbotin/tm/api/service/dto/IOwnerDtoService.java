package com.tsconsulting.dsubbotin.tm.api.service.dto;

import com.tsconsulting.dsubbotin.tm.dto.AbstractOwnerEntityDTO;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IOwnerDtoService<E extends AbstractOwnerEntityDTO> extends IDtoService<E> {

    @NotNull
    List<E> findAll(@NotNull String userId) throws AbstractException;

    @NotNull
    List<E> findAll(@NotNull String userId, @Nullable String sort) throws AbstractException;

    @NotNull
    E findById(@NotNull String userId, @NotNull String id) throws AbstractException;

    @NotNull
    E findByIndex(@NotNull String userId, int index) throws AbstractException;

    @NotNull
    E findByName(@NotNull String userId, @NotNull String name) throws AbstractException;

    void clear(@NotNull String userId) throws AbstractException;

}