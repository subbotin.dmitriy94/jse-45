package com.tsconsulting.dsubbotin.tm.repository.dto;

import com.tsconsulting.dsubbotin.tm.api.repository.dto.IOwnerDtoRepository;
import com.tsconsulting.dsubbotin.tm.dto.AbstractOwnerEntityDTO;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public abstract class AbstractOwnerDtoRepository<E extends AbstractOwnerEntityDTO> extends AbstractDtoRepository<E> implements IOwnerDtoRepository<E> {

    public AbstractOwnerDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void create(@NotNull final String userId, @NotNull final E entity) {
        entity.setUserId(userId);
        entityManager.persist(entity);
    }

}
