package com.tsconsulting.dsubbotin.tm.repository.dto;

import com.tsconsulting.dsubbotin.tm.api.repository.dto.ISessionDtoRepository;
import com.tsconsulting.dsubbotin.tm.dto.SessionDTO;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import java.util.List;

public class SessionDtoRepository extends AbstractDtoRepository<SessionDTO> implements ISessionDtoRepository {

    public SessionDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void open(@NotNull final SessionDTO session) {
        entityManager.persist(session);
    }

    @Override
    public void close(@NotNull final SessionDTO session) {
        entityManager.remove(session);
    }

    @Override
    public boolean contains(@NotNull final SessionDTO session) {
        return entityManager
                .createQuery("FROM SessionDTO WHERE id = :id", SessionDTO.class)
                .setParameter("id", session.getId())
                .getResultStream()
                .findFirst().isPresent();
    }

    @Override
    public boolean existByUserId(@NotNull String id) {
        return entityManager
                .createQuery("FROM SessionDTO WHERE userId = :userId", SessionDTO.class)
                .setParameter("userId", id)
                .getResultStream()
                .findFirst().isPresent();
    }

    @NotNull
    @Override
    public List<SessionDTO> findAll() {
        return entityManager.createQuery("FROM SessionDTO", SessionDTO.class).getResultList();
    }

    @Override
    public @NotNull SessionDTO findById(@NotNull final String id) throws AbstractException {
        return entityManager
                .createQuery("FROM SessionDTO WHERE id = :id", SessionDTO.class)
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public SessionDTO findByIndex(final int index) throws AbstractException {
        return entityManager
                .createQuery("FROM SessionDTO", SessionDTO.class)
                .setMaxResults(1)
                .setFirstResult(index)
                .getResultStream()
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

}
